<?php


class School
{

    private $id;
    private $nom;
    private $lieu;
    private $diplome;
    private $discipline;
    private $localisation;
    private $site;


    public function __construct($id,$nom, $lieu, $diplome, $discipline,$localisation,$site)
    {
        $this->id=$id;
        $this->nom = $nom;
        $this->lieu = str_replace("D0","",$lieu);
        $this->diplome = $diplome;
        $this->discipline = $discipline;
        $this->localisation=$localisation;
        $this->site=$site;
    }

    /**
     * @return mixed
     */

    public function cloneSchool(){
        return new School($this->id,$this->nom,$this->lieu,$this->diplome,$this->discipline,$this->localisation,$this->site);
    }

    public function introduceSchool(){
        echo "Ecole : ".$this->nom." / Lieu : ".$this->lieu." / Type de diplome : ".$this->diplome." /discipline : ".$this->discipline." / SITE : ".$this->localisation.'<br>';
    }

    public function getNom()
    {
        return $this->nom;
    }


    /**
     * @return mixed
     */
    public function getLieu()
    {
        return $this->lieu;
    }

    /**
     * @return mixed
     */
    public function getDiplome()
    {
        return $this->diplome;
    }

    /**
     * @return mixed
     */
    public function getDiscipline()
    {
        return $this->discipline;
    }


    public function getKey(){
        return $this->nom."".$this->lieu."".$this->diplome."".$this->discipline;
    }

    /**
     * @return mixed
     */
    public function getSite()
    {
        return $this->site;
    }

    /**
     * @return mixed
     */
    public function getLocalisation()
    {
        return $this->localisation;
    }

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $site
     */
    public function setSite($site)
    {
        $this->site = $site;
    }

    /**
     * @param mixed $localisation
     */
    public function setLocalisation($localisation)
    {
        $this->localisation = $localisation;
    }

}