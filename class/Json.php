<?php


class Json
{

    private $allSchool;
    private $JsonFile = "https://data.enseignementsup-recherche.gouv.fr/api/records/1.0/search/?dataset=fr-esr-principaux-diplomes-et-formations-prepares-etablissements-publics&rows=500&sort=-rentree_lib&facet=rentree_lib&facet=etablissement_lib&facet=sect_disciplinaire_lib&facet=diplome_rgp&refine.rentree_lib=2017-18&refine.localisation_ins=%C3%8Ele-de-France&field=etablissement_lib, dep_etab,sect_disciplinaire_lib,diplome_rgp";
    private $JsonFile2 = "https://data.enseignementsup-recherche.gouv.fr/api/records/1.0/search/?dataset=fr-esr-principaux-etablissements-enseignement-superieur&sort=uo_lib&facet=uai&facet=type_d_etablissement&facet=com_nom&facet=dep_nom&facet=aca_nom&facet=reg_nom&facet=pays_etranger_acheminement&fields=uai,coordonnees,element_wikidata";

    /**
     * Json constructor.
     * @param $allSchool
     */
    public function __construct()
    {
        $this->allSchool = array();
    }

    /**
     * @return array
     */
    public function getAllSchool()
    {
        return $this->allSchool;
    }

    public function setAllSchool()
    {
        $JsonContent = file_get_contents($this->JsonFile);

        $data = json_decode($JsonContent, true);
        $JsonContent2 = file_get_contents($this->JsonFile2);
        $data2 = json_decode($JsonContent2, true);
        //var_dump($data2);


        foreach ($data["records"] as $res) {
            $ecole = new School($res["fields"]["etablissement"], $res["fields"]["etablissement_lib"], $res["fields"]["com_etab"], $res["fields"]["diplome_rgp"], $res["fields"]["sect_disciplinaire_lib"], null, null);

            /*
                        foreach ($data2["records"] as $rep) {
                            //echo $rep["fields"]["uai"]."<br>";


                            //echo $ecole->getNom()."  ".$ecole->getId()."     ".$rep["fields"]["uai"]."<br>";

                            if ($ecole->getId() != $rep["fields"]["uai"]){

                                break;
                            }

                            $ecole->setLocalisation($rep["fields"]["coordonnees"]);
                            $ecole->setSite($rep["fields"]["element_wikidata"]);

                            //echo $ecole->getLocalisation()."        T <br> ";

                        }
            */
            //$ecole->introduceSchool();
            $allSchool[] = $ecole->cloneSchool();

        }

        unset($ecole);


        $this->allSchool = $allSchool;

    }

    public function optionOnAllDiscipline()
    {
        $JsonContent = file_get_contents($this->JsonFile);

        $data = json_decode($JsonContent, true);
        foreach ($data["facet_groups"][1]["facets"] as $res) {

            echo "<option value=\"" . $res["name"] . "\">" . $res["name"] . "</option>";

        }

    }

    public function optionOnAllDiplome()
    {
        $JsonContent = file_get_contents($this->JsonFile);

        $data = json_decode($JsonContent, true);
        foreach ($data["facet_groups"][2]["facets"] as $res) {

            echo "<option value='" . $res["name"] . "'>" . $res["name"] . "</option>";

        }
    }


}