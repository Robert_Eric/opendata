<?php
include ("apacheConf.php");
?>
<!DOCTYPE html>
<html lang="fr">
<head>
    <link rel="stylesheet" type="text/css" href="<?php echo $URL;?>style.css">
    <meta charset="UTF-8">
    <title>Index.php</title>
</head>

<body>

<?php
include("Header_And_Footer/Header.php");
require_once("class/School.php");
require_once("class/Json.php");

?>


<! -- BODY -->
<div class="center">

    <form action=" <?php echo $URL; ?>/listSchool.php" method="post">

        <p>Recherche par nom d'établissement</p>
        <input type="text" name="etablissement">

        <input type="submit">
    </form>

    <br>

    <form action=" <?php echo $URL; ?>/listSchool.php" method="post">

        <p>Recherche par Discipline</p>
        <select name="discipline" required>
            <option value=""></option>

            <?php
            $j = new Json();
            $j->optionONAllDiscipline();
            ?>

        </select>


        <br>

        <p>Recherche par Département</p>
        <input type="number" name="departement" required>
        <br>

        <p>Recherche par Diplôme</p>
        <select name="diplome" required>
            <option value=""></option>
            <?php
            $j->optionOnAllDiplome();
            ?>

        </select>

        <br>
        <br>
        <input type="submit" value="Chercher">

        <br>


    </form>

</div>

<?php
include("Header_And_Footer/Footer.php");
?>

</body>
</html>