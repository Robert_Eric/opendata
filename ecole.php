<?php
include ("apacheConf.php");
?>
<!DOCTYPE html>
<html lang="fr">
<head>
    <link rel="stylesheet" type="text/css" href="style.css">
    <meta charset="UTF-8">
    <title>Index.php</title>
</head>

<body>

<?php
include("Header_And_Footer/Header.php");
?>


<! -- BODY -->
<div class="center">

    <div class="ecoleCenter">
        <a>
            <h1> Avez vous entendu parler d'une école ?</h1>
            <h5> Faîte vos recherche</h5>

        </a>

    </div>

    <div class="ecoleMap">
        <?php
        include("map/map.html");
        ?>


    </div>

    <div class="ecoleAffiche">
        <form action="ecole.php" method="post">

            <input type="search" name="search" placeholder="Entrer votre école">
            <input type="submit" name="button" value="Rechercher" onclick="">


        </form>

        <?php

        if (isset($_POST["search"]) AND strlen($_POST["search"]) > 0) {


            echo "<table bgcolor='#f0f8ff' border='3' align='center'>
        <tr>
            <th><strong>Ecole</strong></th>

        </tr>";


            $JsonFile = file_get_contents("https://data.enseignementsup-recherche.gouv.fr/api/records/1.0/search/?dataset=fr-esr-principaux-diplomes-et-formations-prepares-etablissements-publics&rows=500&sort=-rentree_lib&facet=rentree_lib&facet=etablissement_type2&facet=etablissement_type_lib&facet=etablissement&facet=etablissement_lib&facet=champ_statistique&facet=dn_de_lib&facet=cursus_lmd_lib&facet=diplome_rgp&facet=diplome_lib&facet=typ_diplome_lib&facet=diplom&facet=niveau_lib&facet=disciplines_selection&facet=gd_disciscipline_lib&facet=discipline_lib&facet=sect_disciplinaire_lib&facet=spec_dut_lib&facet=localisation_ins&facet=com_etab&facet=com_etab_lib&facet=uucr_etab&facet=uucr_etab_lib&facet=dep_etab&facet=dep_etab_lib&facet=aca_etab&facet=aca_etab_lib&facet=reg_etab&facet=reg_etab_lib&facet=com_ins&facet=com_ins_lib&facet=uucr_ins&facet=dep_ins&facet=dep_ins_lib&facet=aca_ins&facet=aca_ins_lib&facet=reg_ins&facet=reg_ins_lib&refine.etablissement_type2=Universit%C3%A9&refine.localisation_ins=%C3%8Ele-de-France&fields=etablissement_lib");
            $data = json_decode($JsonFile, true);

            $repete = array();
            foreach ($data["records"] as $res) {

                if(in_array($res["fields"]["etablissement_lib"],$repete)){
                    continue;
                }
                else{
                    echo '<tr><td>' . $res["fields"]["etablissement_lib"] . '</td></tr>';
                    $repete[]=$res["fields"]["etablissement_lib"];
                }

            }

        }
        /*
        <table bgcolor="#f0f8ff" border="3"></table>
        <tr>
            <th><strong>Ecole</strong></th>
            <th><strong>Diplome</strong></th>
            <th><strong>Type de formation</strong></th>
        </tr>
        */
        ?>
        </table>
    </div>


</div>


<?php
include("Header_And_Footer/Footer.php");
?>
</body>
</html>