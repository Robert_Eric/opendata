
<!DOCTYPE html>
<html lang="fr">
<head>
    <link rel="stylesheet" type="text/css" href="../style.css">
    <meta charset="UTF-8">
    <title>Index.php</title>
</head>

<body>
<?php
include("Header_And_Footer/Header.php");
require_once("class/School.php");
require_once("class/Json.php");
?>
<div class="center">
    <div class="ecoleCenter">
        <?php

        //echo $_POST["etablissement"] . "<br>";
        //echo $_POST["discipline"] . "<br>";
        //echo $_POST["departement"] . "<br>";
        //echo $_POST["diplome"]."<br>";


        $j = new Json();
        //Array de School
        $j->setAllSchool();
        $liste = $j->getAllSchool();

        $toEcho = array();

        foreach ($liste as $ecole) {
            //echo $_POST["diplome"]."        ".$ecole->getDiplome()."<br>";
            //echo $_POST["discipline"]."        ".$ecole->getDiscipline()."<br>";
            //echo $_POST["departement"]."    ".substr($ecole->getLieu(),0,2)."<br>";

            if (isset($_POST["etablissement"]) == false) {
                if (substr($ecole->getLieu(),0,2) == $_POST["departement"] && $ecole->getDiplome() == $_POST["diplome"] && $ecole->getDiscipline() == $_POST["discipline"]) {
                    $toEcho[$ecole->getKey()] = $ecole;
                }
            } else {
                if (strpos($ecole->getNom(), $_POST["etablissement"]) !== false) {
                    $toEcho[$ecole->getKey()] = $ecole;
                }
            }
        }

        usort($toEcho, function ($a, $b) {
            return strcmp($a->getNom(), $b->getNom());
        });


        if (sizeof($toEcho) == 0) {
            echo "<p>Nous ne trouvons pas d'école pour vos recherche. Réessayer en cliquant <a href='diplome.php'>ici</a></p>";
        } else {
            echo '
    <table hcellspacing="0" cellpadding="0" border="0" width="325">

        <tr>
            <td>
                <div class="tableau">
                    <table cellspacing="1" cellpadding="1" border="1" width="820">
                        <tr style="color:white;background-color:grey">
                            <th>Nom Etablissement</th>
                            <th>Département</th>
                            <th>Dîplome</th>
                            <th>Formation</th>
                        </tr>
                            ';

            if (sizeof($toEcho) != 0) {
                foreach ($toEcho as $ecole) {
                    echo "<tr><td>" . $ecole->getNom() . "</td><td>" . $ecole->getLieu() . "</td><td>" . $ecole->getDiplome() . "</td><td>" . $ecole->getDiscipline() . "</td>";
                }
            }

            echo '</table>
</div>
</td>
</tr>
</table>
';
            echo "Nombre de résultat : " . sizeof($toEcho);
        }

        ?>
    </div>
    <div class="mapCenter">
        <?php
        if (sizeof($toEcho) !=0) {
            include("map/map.html");
        }
        ?>
    </div>
    <?php
    include("Header_And_Footer/Footer.php");
    ?>

</body>
</html>
